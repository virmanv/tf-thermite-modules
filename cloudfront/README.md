# AWS CloudFront Terraform module

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| aliases | Extra CNAMEs (alternate domain names), if any, for this distribution. | `list(string)` | `null` | no |
| cache\_behavior | The map of cache behaviors for this distribution. Key `default` will be used as the default cache behavior, all other keys will be used as ordered list of cache behaviors. List from top to bottom in order of precedence. The topmost cache behavior will have precedence 0. | `any` | `null` | no |
| comment | Any comments you want to include about the distribution. | `string` | `null` | no |
| create\_distribution | Controls if CloudFront distribution should be created | `bool` | `true` | no |
| create\_origin\_access\_identity | Controls if CloudFront origin access identity should be created | `bool` | `false` | no |
| custom\_error\_response | One or more custom error response elements | `any` | `{}` | no |
| default\_root\_object | The object that you want CloudFront to return (for example, index.html) when an end user requests the root URL. | `string` | `null` | no |
| enabled | Whether the distribution is enabled to accept end user requests for content. | `bool` | `true` | no |
| geo\_restriction | The restriction configuration for this distribution (geo\_restrictions) | `any` | `{}` | no |
| http\_version | The maximum HTTP version to support on the distribution. Allowed values are http1.1 and http2. The default is http2. | `string` | `"http2"` | no |
| is\_ipv6\_enabled | Whether the IPv6 is enabled for the distribution. | `bool` | `null` | no |
| logging\_config | The logging configuration that controls how logs are written to your distribution (maximum one). | `any` | `{}` | no |
| origin | One or more origins for this distribution (multiples allowed). | `any` | `null` | no |
| origin\_access\_identities | Map of CloudFront origin access identities (value as a comment) | `map(string)` | `{}` | no |
| origin\_group | One or more origin\_group for this distribution (multiples allowed). | `any` | `{}` | no |
| price\_class | The price class for this distribution. One of PriceClass\_All, PriceClass\_200, PriceClass\_100 | `string` | `null` | no |
| retain\_on\_delete | Disables the distribution instead of deleting it when destroying the resource through Terraform. If this is set, the distribution needs to be deleted manually afterwards. | `bool` | `false` | no |
| tags | A map of tags to assign to the resource. | `map(string)` | `null` | no |
| viewer\_certificate | The SSL configuration for this distribution | `any` | `null` | no |
| wait\_for\_deployment | If enabled, the resource will wait for the distribution status to change from InProgress to Deployed. Setting this tofalse will skip the process. | `bool` | `true` | no |
| web\_acl\_id | If you're using AWS WAF to filter CloudFront requests, the Id of the AWS WAF web ACL that is associated with the distribution. The WAF Web ACL must exist in the WAF Global (CloudFront) region and the credentials configuring this argument must have waf:GetWebACL permissions assigned. If using WAFv2, provide the ARN of the web ACL. | `string` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| this\_cloudfront\_distribution\_arn | The ARN (Amazon Resource Name) for the distribution. |
| this\_cloudfront\_distribution\_caller\_reference | Internal value used by CloudFront to allow future updates to the distribution configuration. |
| this\_cloudfront\_distribution\_domain\_name | The domain name corresponding to the distribution. |
| this\_cloudfront\_distribution\_etag | The current version of the distribution's information. |
| this\_cloudfront\_distribution\_hosted\_zone\_id | The CloudFront Route 53 zone ID that can be used to route an Alias Resource Record Set to. |
| this\_cloudfront\_distribution\_id | The identifier for the distribution. |
| this\_cloudfront\_distribution\_in\_progress\_validation\_batches | The number of invalidation batches currently in progress. |
| this\_cloudfront\_distribution\_last\_modified\_time | The date and time the distribution was last modified. |
| this\_cloudfront\_distribution\_status | The current status of the distribution. Deployed if the distribution's information is fully propagated throughout the Amazon CloudFront system. |
| this\_cloudfront\_distribution\_trusted\_signers | List of nested attributes for active trusted signers, if the distribution is set up to serve private content with signed URLs |
| this\_cloudfront\_origin\_access\_identities | Map of origin access identities created |
| this\_cloudfront\_origin\_access\_identity\_iam\_arns | List of IAM arns of the origin access identities created |
| this\_cloudfront\_origin\_access\_identity\_ids | List of IDS of the origin access identities created |